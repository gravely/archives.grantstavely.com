#!/usr/bin/python

__author__ = 'Grant Stavely, grant@grantstavely.com'
__version__ = '0.5'
__date__ = '04/21/2013'

import re
import os
import sys
import yaml
import argparse
import logging
import feedparser
import pprint
from time import strftime
import unidecode
import string
pp = pprint.PrettyPrinter(indent=4)


def slugify(str):
    str = unidecode.unidecode(str).lower()
    str = str.translate(None, string.punctuation)
    return re.sub(r'\W+', '-', str)


def deflickr(str):
    return re.sub('\.? on Flickr\.', '', str)


class Config(object):

    def __init__(self):
        self.log = logging.getLogger('feeder')
        self.log.setLevel(logging.DEBUG)
        self.config = self.dotfile_parse()
        self.arg_parse()
        self.init_log()
        self.init_console()

    def dotfile_parse(self):
        config_file = '~/.feeder'
        user_home_dir = re.compile('^~')
        if user_home_dir.match(config_file):
            config_file = os.path.expanduser(config_file)
        try:
            with open(config_file, 'r') as config_handle:
                config = yaml.safe_load(config_handle.read())
        except IOError as e:
            print('Error reading config file: %s' % e)
            sys.exit('Cowardly refusing to continue unconfigured.')
        except yaml.parser.ParserError as e:
            print('Unable to parse config: %s, parser error %s' % (
                  config_file, e))
            sys.exit('Cowardly refusing to continue unconfigured.')
        return config

    def arg_parse(self):
        parser = argparse.ArgumentParser(
            description='Collect syndicated content and octopress it.')
        parser.add_argument(
            '-u',
            '--url',
            type=str,
            nargs='+',
            metavar='URLs',
            help='fetch content from URLs')
        parser.add_argument(
            '-v',
            '--verbose',
            action='count',
            default=0,
            help='enable verbose output, -vv for more, -vvv for debug')
        args = parser.parse_args()
        self.config.update(vars(args))

    def init_console(self):
        console_format = logging.Formatter(
            '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        console_log = logging.StreamHandler()
        console_log.setFormatter(console_format)
        if self.config['verbose'] == 1:
            console_log.setLevel(logging.WARN)
        elif self.config['verbose'] > 1:
            console_log.setLevel(logging.DEBUG)
        else:
            console_log.setLevel(logging.ERROR)
        self.log.addHandler(console_log)

    def init_log(self):
        print('Logging to %s' % self.config['log_file'])
        self.log_file = logging.FileHandler(self.config['log_file'])
        log_format = logging.Formatter(
            '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        self.log_file.setFormatter(log_format)
        self.log_file.setLevel(logging.DEBUG)
        self.log.addHandler(self.log_file)

# http://wiki.python.org/moin/RssLibraries

config = Config()

for feed in config.config['feeds']:
    feed = feedparser.parse(feed['url'])
    for item in feed['items']:
        date = strftime("%Y-%m-%d %H:%M", item['published_parsed'])
        title = deflickr(item['title'])
        slug = slugify(title)
        post = deflickr(item['summary'])
        #pp.pprint(item)
        print("""
---
layout: post
title: %s
date: %s
permalink: /%s
comments: false
---
%s
""" % (title, date, slug, post))
